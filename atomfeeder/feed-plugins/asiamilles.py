#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

FEED_INFO = {
        "base_url": "http://redeem.asiamiles.com/zh/electronics-and-accessories.html",
        "feed_xpath": {
            "title_xpath": "//html/head//title/text()",
            "entry_xpath": "//body//div[@class='category-products']/ul[@class='products-grid']/li[contains(concat(' ', @class, ' '), ' item ')]",
            },
        "entry_xpath": {
            "link_xpath": ".//h2[@class='product-name']/a[1]/@href",
            "title_xpath": ".//h2[@class='product-name']/a[1]/@title",
            "content_xpath": "./*",
            },
        "entry_attribute": {
            "date_format": ["%m-%d %H:%M",],
            "content_type": "xhtml",
            },
    }

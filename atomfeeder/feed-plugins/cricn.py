#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

FEED_INFO = {
        "base_url": "http://news.cri.cn/roll",
        "encoding": "UTF-8",  # default to UTF-8
        "feed_xpath": {
            "title_xpath": "//html/head//title/text()",
            "entry_xpath": "//body//div[contains(@class, 'list-box')]//ul/li[contains(concat(' ', @class, ' '), ' list-item ')]",
            },
        "entry_xpath": {
            "link_xpath": ".//div[@class='list-title']/a[@target='_blank']/@href",
            "title_xpath": ".//div[@class='list-title']/a[@target='_blank']//text()",
            "updated_xpath": ".//div[@class='list-time']/text()",
            },
        "entry_attribute": {
            "date_format": ["%Y-%m-%d %H:%M",],
            "time_zone": "+08:00",
            },
    }

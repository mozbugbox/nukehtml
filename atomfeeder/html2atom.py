#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

"""
The Atom Syndication Format
https://tools.ietf.org/html/rfc4287
"""

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import time
import logging as log

NATIVE=sys.getfilesystemencoding()

import re
import datetime
from atomfeeder import atomfeed

import lxml.html
from lxml import etree
from lxml.html.soupparser import fromstring as fromstring_soup
from lxml.html import fromstring

def updated_now():
    """create updated tag from current time."""
    now = time.gmtime()
    str_t = time.strftime("%Y-%m-%dT%H:%M:%SZ", now)
    return str_t

def xpath2string(root, xp, as_xhtml=False):
    """convert xpath to text"""
    try:
        nodes = root.xpath(xp)
    except lxml.etree.XPathEvalError as e:
        log.error(f"{e}: {xp}")
        raise
    res = None
    if len(nodes) > 0:
        if as_xhtml:
            # Assume a list of element node
            res = [lxml.html.tostring(x, encoding="unicode", method="xml", pretty_print=True)
                    for x in nodes]
            res = "\n".join(res)
        else:
            if isinstance(nodes, list):
                node = nodes[0]
                if isinstance(node, lxml.html.HtmlElement):
                    res = node.text_content()
                elif isinstance(node, str):
                    res = " ".join([x.strip() for x in nodes])
                    res = res.strip()
            elif isinstance(nodes, str):
                res = nodes.strip()
    return res

def parse_chinese_date(date_str):
    """Parse some know chinese datetime strings"""
    result = None
    rfc3339_format = "%Y-%m-%dT%H:%M:%SZ"
    if date_str == "刚刚":
        result = time.strftime(rfc3339_format, time.localtime())
    elif "分钟前" in date_str:
        mobj = re.search("(\d+)分钟前", date_str)
        if mobj:
            minute = int(mobj.group(1))
            item_time = datetime.datetime.now() - datetime.timedelta(minutes=minute)
            result = item_time.strftime(rfc3339_format)
    elif "小时前" in date_str:
        mobj = re.search("(\d+)小时前", date_str)
        if mobj:
            hour = int(mobj.group(1))
            item_time = datetime.datetime.now() - datetime.timedelta(hours=hour)
            result = item_time.strftime(rfc3339_format)
    # log.info(f"{date_str=} {result=}")
    return result

class EntryGen:
    """
        class_variable:
            date_format: date format for strptime()
    """
    id_xpath = None
    link_xpath = None
    title_xpath = None
    author_xpath = None
    updated_xpath = None
    summary_xpath = None
    content_xpath = None

    date_format = None
    time_zone = None
    content_type = "text"

    def __init__(self, node):
        self.root = node
        self._updated = None

    def _get_xpath(self, name, as_xhtml=False):
        """Get xpath content as text/html"""
        res = None
        xpath_name = name + "_xpath"
        xpath_v = getattr(self, xpath_name, None)
        if xpath_v:
            xpath_list = [xpath_v] if isinstance(xpath_v, str) else xpath_v
            for apath in xpath_list:
                res = xpath2string(self.root, apath, as_xhtml)
                if res is not None:
                    break
        return res

    @property
    def id(self):
        eid = self._get_xpath("id")
        return eid

    @property
    def link(self):
        link = self._get_xpath("link")
        return link

    @property
    def title(self):
        title = self._get_xpath("title")
        return title

    @property
    def author(self):
        author = self._get_xpath("author")
        return author

    def _format_date(self, date_str):
        date_normal = None

        rfc3339_format = "%Y-%m-%dT%H:%M:%SZ"
        date_format = self.date_format
        time_zone = self.time_zone
        if time_zone:
            rfc3339_format = rfc3339_format.replace("Z", time_zone)

        if date_format:
            atime = None
            if isinstance(date_format, str):
                date_format = [date_format]
            for dformat in date_format:
                try:
                    atime = time.strptime(date_str, dformat)
                    break
                except ValueError:
                    continue
            if atime is not None:
                if atime.tm_year == 1900:  # no year info in date
                    atime = list(atime)
                    now = time.gmtime()
                    atime[0] = now.tm_year
                date_normal = time.strftime(rfc3339_format, tuple(atime))

            if date_normal is None:
                chinese_time = parse_chinese_date(date_str)
                if chinese_time:
                    date_normal = chinese_time

            if date_normal is None:
                mobj = re.match("(\d{2}):(\d{2})", date_str)
                if mobj:
                    hour = int(mobj.group(1))
                    minute = int(mobj.group(2))
                    item_time = datetime.datetime.now()
                    item_time.replace(hour=hour, minute=minute)
                    date_normal = item_time.strftime(rfc3339_format)

            if date_normal is None:
                log.warn("Failed to parse date: " + date_str)
                date_normal = date_str

        return date_normal

    @property
    def updated(self):
        updated = self._get_xpath("updated")
        if updated is not None:
            updated = self._format_date(updated)
        else:
            if self._updated is None:
                self._updated = updated_now()
            updated = self._updated

        return updated

    @property
    def summary(self):
        summary = self._get_xpath("summary")
        return summary

    @property
    def content(self):
        content_type = self.content_type
        if content_type in ["html", "xhtml"]:
            as_xhtml = True
        else:
            as_xhtml = False
        content = self._get_xpath("content", as_xhtml)
        if content is None:
            content = self.title
        #print(content); sys.exit()
        return content

class Html2Atom:
    """
    entry, title, author + _xpath

    """
    entry_xpath = None
    title_xpath = None
    author_xpath = None

    base_url = None

    def __init__(self, content, base_url=None, encoding=None):
        if base_url is None:
            base_url = self.base_url
        self.base_url = base_url

        try:
            self.xml = xml = fromstring_soup(content)
        except:
            self.xml = xml = fromstring(content)

        try:
            xml.make_links_absolute(base_url)
        except TypeError as e:
            log.error(str(e))

    @property
    def atom(self):
        title = self.title
        author = self.author
        updated = self.updated
        feed = atomfeed.Feed(author, title, updated)
        if self.base_url:
            feed.link.append(atomfeed.Link(self.base_url))
        entries = self.entries
        log.debug("entries: {}".format(entries))

        entry_list = []
        for node in entries:
            egen = self.EntryGenKlass(node)
            entry = atomfeed.Entry(egen.author, egen.link, egen.summary,
                    egen.title, egen.updated, egen.id)
            entry_list.append(entry)

            e_summary = egen.summary
            e_content = egen.content
            if e_summary is not None:
                entry.summary = e_summary
            if e_content is not None:
                content_type = egen.content_type
                entry.content = atomfeed.Content(e_content, content_type)
            feed.entry.append(entry)

        return feed

    @property
    def title(self):
        res = xpath2string(self.xml, self.title_xpath)
        return res

    @property
    def id(self):
        return self.base_url

    @property
    def updated(self):
        str_t = updated_now()
        return str_t

    @property
    def entries(self):
        elms = self.xml.xpath(self.entry_xpath)
        return elms

    @property
    def author(self):
        author_xpath = self.author_xpath
        if author_xpath:
            txt = self.xml.xpath(self.author_xpath)
        else:
            txt = "html2atom"
        return txt

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    fname = sys.argv[1]
    with io.open(fname) as fh:
        content = fh.read()
    base_url = 'http://news.cri.cn/roll'

    atomgen = Html2Atom(content, base_url)
    atomgen.title_xpath = "/html/head/title"
    atomgen.entry_xpath = "//div[@class='main']/div[@class='list-box']//ul/li[contains(concat(' ', @class, ' '), ' list-item ')]"

    # id, link, title, author, updated, summary, content + _xpath
    class MyEntryGen(EntryGen):
        link_xpath = ".//h4[@class='tit']/a[@target='_blank']/@href"
        title_xpath = ".//h4[@class='tit']/a[@target='_blank']//text()"
        updated_xpath = ".//h4[@class='tit']/i/text()"
        date_format = "%m-%d %H:%M"

    atomgen.EntryGenKlass = MyEntryGen
    print(str(atomgen.atom))

if __name__ == '__main__':
    main()


#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log

NATIVE=sys.getfilesystemencoding()

HEAD = """\
<?xml version="1.0" encoding="utf-8"?>

"""

import uuid
import lxml.html
from lxml import etree

def create_node(tag, value, root):
    if not value:
        return

    kid = None
    if isinstance(value, str):
        kid = etree.Element(tag)
        kid.text = str(value)
    elif isinstance(value, AtomNode):
        kid = value.generate_node(tag)
    elif isinstance(value, list):
        for sv in value:
            create_node(tag, sv, root)
    else:
        raise ValueError("Wrong value type for {}: {} [{}]".format(
            tag, value, type(value)))

    if kid is not None:
        root.append(kid)

class AtomNode:
    _node_names = []
    _attrib_names = []
    _root_name = ""
    _text = None
    def generate_node(self, root_name=None):
        if root_name is None:
            root_name = self._root_name
        root = etree.Element(root_name)

        for attrname in self._attrib_names:
            v = getattr(self, attrname, None)
            if v is not None:
                root.attrib[attrname] = v

        if self._text is not None:
            v = getattr(self, self._text, None)
            if v is not None:
                root.text = v

        for nodename in self._node_names:
            v = getattr(self, nodename)
            create_node(nodename, v, root)

        return root

    def __str__(self):
        root = self.generate_node()
        txt = etree.tostring(root, encoding="UTF-8", pretty_print=True,
                xml_declaration=True)
        return txt.decode("UTF-8")

class Content(AtomNode):
    """Content Construct"""
    _attrib_names = ["type"]
    _text = "text"
    _root_name = "content"
    def __init__(self, content, content_type="text"):
        self.text = content
        self.type = content_type

    def generate_node(self, root_name=None):
        node = super().generate_node(root_name)
        if self.type == "xhtml":
            xhtml_header = '<div xmlns="http://www.w3.org/1999/xhtml">'
            text = self.text
            if not text.lstrip().startswith(xhtml_header):
                text = "{}\n{}\n</div>".format(xhtml_header, text)
            xhtml_node = lxml.html.fromstring(text)
            node.append(xhtml_node)
            node.text = ""
        return node

class Link(AtomNode):
    _attrib_names = ["href", "rel", "type", "hreflang", "title", "length"]
    _root_name = "link"
    def __init__(self, href):
        self.href = href
        self.rel = None
        self.type = None
        self.hreflang = None
        self.title = None
        self.length = None

class Person(AtomNode):
    _node_names = ["name", "uri", "email"]
    _root_name = "person"
    def __init__(self, name):
        self.name = name
        self.uri = None
        self.email = None

class Entry(AtomNode):
    _node_names = ["author", "category", "content", "contributor", "id",
            "link", "published", "rights", "source", "summary", "title",
            "updated"]
    _root_name = "entry"
    def __init__(self, author, alt_url, summary, title, updated, feed_id):
        author = Person(author)
        self.author = [author]
        self.category = []
        self.content = None
        self.contributor = []
        self.id = feed_id
        link = Link(alt_url)
        link.rel = "alternate"
        self.link = [link]
        self.published = None
        self.rights = None
        self.source = None
        self.summary = summary
        self.title = title
        self.updated = updated

    def generate_node(self, root_name=None):
        if self.id is None:
            if len(self.link) > 0:
                u = uuid.uuid3(uuid.NAMESPACE_URL, str(self.link[0]))
                self.id = u.urn
        node = super().generate_node(root_name)
        return node

class Feed(AtomNode):
    _node_names = ["author", "category", "contributor", "generator", "icon",
            "id", "link", "logo", "rights", "subtitle", "title", "updated",
            "entry"]
    _root_name = "feed"
    _attrib_names = ["xmlns"]
    xmlns = "http://www.w3.org/2005/Atom"

    def __init__(self, author, title, updated):
        self.author = [Person(author)]
        self.category = []
        self.contributor = []
        self.generator = None
        self.icon = None
        self.id = None
        self.link = []
        self.logo = None
        self.rights = None
        self.subtitle = None
        self.title = title
        self.updated = updated
        self.entry = []

    def generate_node(self, root_name=None):
        if self.id is None:
            if len(self.link) > 0:
                u = uuid.uuid3(uuid.NAMESPACE_URL, str(self.link[0]))
                self.id = u.urn
        node = super().generate_node(root_name)
        return node

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

if __name__ == '__main__':
    main()


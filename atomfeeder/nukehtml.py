#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log
from atomfeeder import html2atom

NATIVE=sys.getfilesystemencoding()

__version__ = "0.1.0"
APPNAME = "nukehtml"
FEED_PLUGIN_DIR = "feed-plugins"

DEBUG = False
def setup_plugin_path():
    """Setup plugin searching path"""
    pathes = []

    script_dir = os.path.dirname(sys.argv[0])
    pathes.append(script_dir)

    data_dir = os.path.join("$HOME", ".local", "share", APPNAME)
    data_dir = os.path.expandvars(data_dir)
    pathes.append(data_dir)

    real_path = os.path.realpath(sys.argv[0])
    real_dir = os.path.dirname(real_path)
    if real_dir not in pathes:
        pathes.append(real_dir)

    package_dir = os.path.dirname(__file__)
    if package_dir not in pathes:
        pathes.append(package_dir)

    pathes = [os.path.join(x, FEED_PLUGIN_DIR) for x in pathes]
    pathes = [os.path.abspath(x) for x in pathes]
    log.debug("Plugin search path: ['{}']".format("', '".join(pathes)))
    return pathes

def check_permission(path):
    """ Check permission on path as user/root writable
    Make sure plugin and its dir not writable by user other than owner/root
    """
    uid = os.geteuid()
    dirname = os.path.dirname(path)
    for p in [dirname, path]:
        dir_stat = os.stat(p)
        if dir_stat.st_uid not in {0, uid}:
            msg = "Wrong owner for plugin"
            if os.path.isdir(p):
                msg += " directory"
            raise PermissionError("{}: {}".format(msg, p))
        if dir_stat.st_mode & 0o022:
            msg = "Wrong permission for plugin"
            if os.path.isdir(p):
                msg += " directory"
            raise PermissionError("{}: {}".format(msg, p))

def load_module(mode_name):
    mod = None

    if not mode_name.endswith(".py"):
        mode_name = mode_name + ".py"

    pathes = setup_plugin_path()
    for p in pathes:
        path_full = os.path.join(p, mode_name)
        if os.path.exists(path_full):
            check_permission(path_full)
            log.debug("load plugin module: {}".format(path_full))
            import importlib.util
            spec = importlib.util.spec_from_file_location("feedinfo",
                    path_full)
            mod = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(mod)
            break
    return mod

def setup_entry_klass(plugin):
    """setup EntryGen class with info from plugin"""
    feed_info = plugin.FEED_INFO
    egen_klass = getattr(plugin, "EntryGen", None)
    if egen_klass is None:
        class MyEntryGen(html2atom.EntryGen):
            pass
        egen_klass = MyEntryGen
        for k, v in feed_info["entry_xpath"].items():
            if not k.endswith("_xpath"):
                k = k + "_xpath"
            setattr(egen_klass, k, v)
        for k, v in feed_info["entry_attribute"].items():
            setattr(egen_klass, k, v)
    return egen_klass

def generate_atom(content, plugin, base_url, encoding):
    feed_info = plugin.FEED_INFO
    atomgen = html2atom.Html2Atom(content, base_url, encoding=encoding)
    for k, v in feed_info["feed_xpath"].items():
        if not k.endswith("_xpath"):
            k = k + "_xpath"
        setattr(atomgen, k, v)

    egen_klass = setup_entry_klass(plugin)
    atomgen.EntryGenKlass = egen_klass
    return atomgen

def setup_arg_parser():
    import argparse
    parser = argparse.ArgumentParser(
            description="Convert HTML page to Atom feed")
    parser.add_argument("plugin", nargs=1, help="feed plugin to use")

    content_group = parser.add_mutually_exclusive_group()
    content_group.add_argument("-f", "--filename", action="store",
            help="Load HTML content from filename instead of stdin")
    content_group.add_argument("-g", "--get-online", action="store_true",
            help="get content from baseurl")

    parser.add_argument("-b", "--baseurl", action="store",
            help="baseurl")
    parser.add_argument("-D", "--debug", action="store_true",
            help="debug run")

    return parser

def main():
    global DEBUG
    parser = setup_arg_parser()
    args = parser.parse_args()

    if args.debug == True:
        DEBUG = True

    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    if DEBUG:
        log_level = log.DEBUG
    else:
        log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    feed_module_filename = args.plugin[0]

    plugin = load_module(feed_module_filename)
    if not plugin:
        log.error("Failed to load feed plugin: {}".format(feed_module_filename))
        sys.exit(2)

    feed_info = plugin.FEED_INFO
    encoding = feed_info.get("encoding", "UTF-8")

    base_url = feed_info.get("base_url", None)
    if args.baseurl:
        base_url = args.baseurl

    if args.get_online:
        import urllib.request
        with urllib.request.urlopen(base_url) as fd:
            content = fd.read()
    elif args.filename:
        with io.open(args.filename, encoding=encoding, errors="replace") as fh:
            content = fh.read()
    else:
        stdin = io.TextIOWrapper(sys.stdin.buffer, encoding=encoding, errors="replace")
        content = stdin.read()
    #print(content); exit()

    if len(content) == 0 or content.isspace():
        log.error("Feed Error. Content is empty: {}".format(base_url))
        return 1

    try:
        atomgen = generate_atom(content, plugin, base_url, encoding)
        print(str(atomgen.atom)) # print Atom to stdout
    except:
        log.error("Feed Error: {}".format(base_url))
        raise

if __name__ == '__main__':
    main()


#!/usr/bin/phantomjs
/*
 * Fetch a web page and print out the DOM content as HTML
 */

var DEBUG = false;

var system = require('system');
var webPage = require('webpage');

console.debug = function() {
  if (DEBUG) {
    system.stderr.write(Array.prototype.join.call(arguments, ' ') + '\n');
  }
};

console.error = function() {
    system.stderr.write(Array.prototype.join.call(arguments, ' ') + '\n');
};

/* Helper function to wait for condition before do callback
 * func: condition checking function
 * callback: called at the end as callback(successed, condition)
 *           where `successed` will be `false` when max_timeout reached.
 */
function waitFor(func, callback, max_timeout) {
  if (!max_timeout) {
    max_timeout = 10000; // 10 sec
  }

  var check_interval = 1000; // in millisec
  var stop = Date.now() + max_timeout;
  var interval_id = setInterval(function() {
    var res = func();
    if (res) {
      clearInterval(interval_id);
      callback(true, res);
    } else {
      if (Date.now() > stop) {
        clearInterval(interval_id);
        callback(false, null);
      }
    }
  }, check_interval);
}

//Quick and dirty string format method
function str_format(pat) {
  var args = [].slice.call(arguments, 1);
  for (var i = 0; i < args.length; i++) {
    if (typeof args[i] !== "string") {
      args[i] = JSON.stringify(args[i], undefined, 4);
    }
    pat = pat.replace("{}", args[i]);
  }
  return pat;
}

// test if page has a node confirms to selector
function has_node(page, selector) {
  var res = page.evaluate(function(sel) {
    var res = document.querySelector(sel);
    return res;
  }, selector);
  return res;
}

function parse_args() {
    var USAGE, argv, options, args, narg, arg, i;
    USAGE = str_format(
      "Fetch a web page and print out the DOM content as HTML\n" +
      "Usage: {} " +
      "[-D | -c config-file | --wait-selector cssSelector | " +
      "--extra-wait seconds] " +
      "<some URL>", system.args[0]);
    argv = system.args.slice(1);
    options = {
        "debug": false,
        "wait_selector": null,
        "extra_wait": null,
        "config": null
    };
    args = [];
    if (argv.length < 1) {
        if (!DEBUG) {
            console.error(USAGE);
            phantom.exit();
        }
    } else {
        narg = argv.length;
        i = 0;
        while (i < narg) {
            arg = argv[i];
            if (arg === "-D") {
                options["debug"] = true;
                DEBUG = true;
            } else if (arg === "-c") {
              i += 1;
              options["config"] = argv[i];
            } else if (arg === "--wait-selector") {
              i += 1;
              options["wait_selector"] = argv[i];
            } else if (arg === "--extra-wait") {
              i += 1;
              options["extra_wait"] = argv[i];
            } else if (arg[0] === "-") {
                console.error(str_format("Unknown options: {}", arg));
                phantom.exit(2);
            } else {
                args.push(arg);
            }
            i += 1;
        }
    }
    if (!DEBUG && args.length === 0) {
        console.error(USAGE);
        console.error("A URL is required");
        phantom.exit(2);
    }
    return [options, args];
}

// After page loaded, do action
function dump_page(page, options) {
  var _do_it = function() {
    var content = page.content;
    console.log(content);
    if (DEBUG) {
      var fname = "web-content.png";
      console.debug("Save page image to:", fname);
      page.render(fname);
    }
    phantom.exit();
  };

  var extra_wait = options["extra_wait"];
  if (extra_wait === null) {
    do_it();
  } else {
    setTimeout(function() { _do_it(); }, extra_wait * 1000);
  }
}

function do_page() {
  var res = parse_args();
  var options = res[0];
  var args = res[1];
  var url = args[0];

  if (options["config"]) {
    try {
      var fs = require("fs");
      var config_str = fs.read(options["config"]);
      //console.debug("config_str", config_str);
      config = JSON.parse(config_str);
      for (var k in config) {
        if (config.hasOwnProperty) {
          options[k.replace(/-/g, "_")] = config[k];
        }
      }
    } catch (e) {
      console.error(e);
      phantom.exit(2);
    }
  }

  var page = webPage.create();
  var wait_callback = function(checked, res) {
    if (checked === true) {
      dump_page(page, options);
    } else {
      console.error('WaitFor Failed');
      if (DEBUG) {
        var fname = "web-content.png";
        console.debug("Save page image to:", fname);
        page.render(fname);
      }
      phantom.exit(1);
    }
  };

  console.debug("options:", JSON.stringify(options));

  var max_wait = 10000;
  page.open(url, function(status) {
    var selector = options["wait_selector"];
    if (!selector) {
      dump_page(page, options);
    } else {
      waitFor(
          function() {
            return has_node(page, selector);
          },
          wait_callback, max_wait);
    }
  });
}

do_page();
// vim:fileencoding=utf-8:sw=2:et:filetype=javascript

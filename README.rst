Nukehtml
========

Convert HTML page to Atom feed. A plugin system is used to handle site specific
scraping.

Plugins use XPATH to scrape web site for items like title, entry, link,
updated, etc.

The web site specific information should be put in a dict named ``FEED_INFO``
inside the plugin. The format of the ``FEED_INFO`` is:

.. code:: python

    #!/usr/bin/python3
    # vim:fileencoding=utf-8:sw=4:et

    FEED_INFO = {
            "base_url": "http://news.cri.cn/roll",
            "encoding": "UTF-8", # default to UTF-8
            "feed_xpath": {
                "title_xpath": "//html/head//title/text()",
                "entry_xpath": "//body//div[@class='main']/div[@class='list-box']//ul/li[contains(concat(' ', @class, ' '), ' list-item ')]",
                },
            "entry_xpath": {
                "link_xpath": ".//h4[@class='tit']/a[@target='_blank']/@href",
                "title_xpath": ".//h4[@class='tit']/a[@target='_blank']//text()",
                "updated_xpath": ".//h4[@class='tit']/i[1]/text()",
                },
            "entry_attribute": {
                "date_format": ["%m-%d %H:%M",],
                "time_zone": "+08:00",
                "content_type": "text",
                },
        }

-  ``feed_xpath``: setup the XPath of ``Html2Atom`` class in module
   ``atomfeeder.html2atom``.

-  ``entry_xpath``: seupt the XPath of the ``EntryGen`` class in the
   same module above.

-  ``entry_attribute``: setup misc attributes of the ``EntryGen`` class.

The possible keys for the ``FEED_INFO`` can be found in
``atomfeeder.html2atom``.

Advanced Plugins
================

If the simple XPath syntax (not really) is not enough for capturing the
information in the HTML content, a plugin could create a class named
``EntryGen`` which subclass the ``atomfeeder.html2atom.EntryGen``. The
``EntryGen`` can perform much more complex manipulation over DOM nodes.

.. code:: python

    #!/usr/bin/python3
    # vim:fileencoding=utf-8:sw=4:et
    from atomfeeder import html2atom

    FEED_INFO = {
            "base_url": "http://news.cri.cn/roll",
            "encoding": "UTF-8", # default to UTF-8
            "feed_xpath": {
                "title_xpath": "//html/head//title/text()",
                "entry_xpath": "//body//div[@class='main']/div[@class='list-box']//ul/li[contains(concat(' ', @class, ' '), ' list-item ')]",
                },
        }
    class EntryGen(html2atom.EntryGen):
        "link_xpath" = ".//h4[@class='tit']/a[@target='_blank']/@href"
        "title_xpath" = ".//h4[@class='tit']/a[@target='_blank']//text()"
        "updated_xpath" = ".//h4[@class='tit']/i[1]/text()"
        "date_format" = ["%m-%d %H:%M",]
        "time_zone" = "+08:00"
        "content_type" = "text"

        @property
        def link(self):
            u_nodes = self._get_xpath("link")
            u = u_nodes[0].replace("view", "discuss")
            return u
        ...

For more informat, please refer to the source of ``atomfeeder.html2atom``.

Plugin Directory
================

The searching directory for the plugin is ``["nukehtml-director/feed-plugins",
"~/.local/share/nukehtml-directory/feed-plugins"]``.

Since the plugins will be loaded as python code, for the security reasons, the
permission of the plugin file and plugin directory have to be *writable only*
by the current user or the root.

Dynamic Web Content
===================

Some web page loads dynamic content through ``XMLHttpRequest``. The content of
this kind of page are not easily accessible.

To dump dynamic web page, a simple ``phantomjs`` script called
``web-content-dump.js`` was created. The web page will be loaded just like
being viewed in a web browser. After loading finished, the content of the web
page will be dumped to the stdout.

.. code:: sh

    web-content-dump.js --wait-selector 'div.SearchResult' http://some.org/dynamic/index.html | nukehtml someOrg

-  ``--wait-selector`` specify a CSS selector. The selector designate that the
   dynamic content was loaded.

-  ``--extra-wait`` extra time in seconds to wait before dump page content.

-  phantomjs_ should be installed before using the ``web-content-dump.js``
   script.

.. _phantomjs: http://phantomjs.org/ 


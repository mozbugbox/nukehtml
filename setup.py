#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
# https://packaging.python.org/en/latest/distributing.html

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
from setuptools import setup, find_packages

NATIVE=sys.getfilesystemencoding()

here = os.path.abspath(os.path.dirname(__file__))

README_FILE = "README.rst"
# Get the long description from the README file
with io.open(os.path.join(here, README_FILE), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='nukehtml',
    version='0.1.0',

    description='HTML to Atom feed converter',
    long_description=long_description,

    # The project's main homepage.
    url='https://gitlab.com/mozbugbox/nukehtml',

    # Author details
    author="mozbugbox",
    author_email='mozbugbox@yahoo.com.au',

    # Choose your license
    license='GPL version 3 or later',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: End Users',
        'Topic :: Text Processing :: Filters',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],

    # What does your project relate to?
    keywords='html atom feed filter converter',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),

    # Alternatively, if you want to distribute just a my_module.py, uncomment
    # this:
    py_modules=["nukehtml"],

    package_data={
        'atomfeeder': ['doc/README.rst'],
    },
    include_package_data=True,

    entry_points={
        'console_scripts': [
            'nukehtml=atomfeeder.nukehtml:main',
        ],
    },
    scripts = ["web-content-dump.js"],
)
